import datetime
from Tkinter import *
# http://content.codersdojo.org/code-kata-catalogue/berlin-clock/
h1sym = "RRRR"
h2sym = "RRRR"
m1sym = "YYRYYRYYRYYR"
m2sym = "YYYY"
off = "OOOOOOOOOOO"
root = Tk()
h1 = StringVar()
h2 = StringVar()
m1 = StringVar()
m2 = StringVar()
s1 = StringVar()
s2 = StringVar()
Label(root, textvariable=h1).pack()
Label(root, textvariable=h2).pack()
Label(root, textvariable=m1).pack()
Label(root, textvariable=m2).pack()
Label(root, textvariable=s1).pack()
Label(root, textvariable=s2).pack()
    
def update():
    now = datetime.datetime.now()
    #nowstr = " %02d %02d %02d " % (now.hour, now.minute, now.second)
    h1str = h1sym[:now.hour / 5] + off:[4-now.hour/5]
    h2str = h2sym[:now.hour % 5]
    m1str = m1sym[:now.minute / 5]
    m2str = m2sym[:now.minute % 5]
    s1str = m1sym[:now.second / 5]
    s2str = m2sym[:now.second % 5]
    h1.set(h1str)
    h2.set(h2str)
    m1.set(m1str)
    m2.set(m2str)
    s1.set(s1str)
    s2.set(s2str)
    root.after(1000,update)  # reschedule event (msec)

root.after(2,update)
root.mainloop()