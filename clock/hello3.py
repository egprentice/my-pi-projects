import datetime
from Tkinter import *
# http://content.codersdojo.org/code-kata-catalogue/berlin-clock/
h1sym = "RRRR"
h2sym = "RRRR"
m1sym = "YYRYYRYYRYYR"
m2sym = "YYYY"
off = "OOOOOOOOOOO"
spaces = "       "
root = Tk()
h1 = StringVar()
h2 = StringVar()
m1 = StringVar()
m2 = StringVar()
s1 = StringVar()
s2 = StringVar()
Label(root, textvariable=h1, font=("Courier", 16), fg="red").pack()
Label(root, textvariable=h2, font=("Courier", 16), fg="red").pack()
Label(root, textvariable=m1, font=("Courier", 16), fg="yellow").pack()
Label(root, textvariable=m2, font=("Courier", 16), fg="yellow").pack()
Label(root, textvariable=s1, font=("Courier", 16), fg="yellow").pack()
Label(root, textvariable=s2, font=("Courier", 16), fg="yellow").pack()
    
def update():
    now = datetime.datetime.now()
    for r in range(3):
        for c in range(4):
            Tkinter.Label(root, text='R%s/C%s'%(r,c),
                borderwidth=1 ).grid(row=r,column=c)
    root.after(1000,update)  # reschedule event (msec)

root.after(2,update)
root.mainloop()