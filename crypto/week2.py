import sys
from Crypto import Random
from Crypto.Cipher import AES

def addPadding(bytesIn):
    lenIn=len(bytesIn)
    lenLast = lenIn%16
    padLen = 16-lenLast
    if (padLen==0):
        PadLen=16
    padSource = ""
    for j in range(16): padSource += chr(j)

    padString = padSource[padLen]*padLen
    #print len(padSource)
    padded = bytesIn + padString
    return padded
    
def removePadding(bytesIn):
    padChar = bytesIn[-1:]
    #print  ord(padChar)
    padLen=2
    unpadded = bytesIn[:-padLen]
    #print len(unpadded)
    return unpadded
    
key = b'Sixteen byte key'
#iv = Random.new().read(AES.block_size)
cipher1 = AES.new(key, AES.MODE_ECB)
ptmsg1 = b'Attack at dawn'
#print len(ptmsg1)
paddedIn = addPadding(ptmsg1)
ctmsg =  cipher1.encrypt(paddedIn)
#iv2 = ctmsg[0:16]
ctmsg2 = ctmsg
cipher2 = AES.new(key, AES.MODE_ECB)
ptmsg2 = cipher2.decrypt(ctmsg2)
ptmsg2a = removePadding(ptmsg2)
#print len(ptmsg2a)
print ptmsg2a

def cbcEncrypt(key,pt):
    iv = Random.new().read(AES.block_size)
    ptpadded = addPadding(pt)
    return encryptR(iv,key,iv,ptpadded
    
def encryptR(acc,key,iv,pt):
    if (len(pt)==0):
        return acc
    else:
        currpt = pt[0:16]
        rest = pt[16:]
        inter = xor(currpt,iv)
        cipher1 = AES.new(key, AES.MODE_ECB)
        currct = cipher1.encrypt(key,inter)
        newacc = acc + currct
        return encryptR(acc + currct, currct, rest)

